include <Fractional_T-Slot_20x_mm_OpenSCAD_Library/T-Slot.scad>
include <general-values.scad>
use <bed-clamps.scad>
use <leadscrew-nut-clamp.scad>
use <z-axis-clamp-and-leadscrew-pillow-block.scad>
use <lib/utl.NEMA.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>

//This is the max Y dimension when the constraining factor are the Z rails, which probably never is
//autoy=external-2*profilewidth-2*z_axis_clamp_x_mountpoint_offset()-bed_clamps_size()[1];

//This is the max Y dimension when the constraining factor is the Z leadscrews
autoy=external-2*profilewidth-nema17_size(nema_length)[0]-leadscrew_nut_d;

lx=bed_platform_length=="auto"?external-2*(45+5):bed_platform_length;
ly=bed_platform_width=="auto"?autoy:bed_platform_width;

disty=min(autoy,bed_platform_width);

if (bed_platform_width<autoy) echo("Warning: build platform too small");

bed_level_slack = 12;
screw3_angle_center = 10; //the distance from the center of the hole to the edge of the aluminum extrusion for the angle that holds the bed on the right.



//print_support_beams();
 bed();

function bed_x_clamps_offset() = lx/3;

module print_support_beams(){
    support_beam();
    translate([-150,150,0]) rotate(90) mirror([0,1,0]) support_beam();
}

module bed(assembly_on_top = true){
    l = assembly_on_top ? lx+2*bedprofile     : bed_platform_length+bedprofile/4;
    w = assembly_on_top ? disty : disty-bedprofile*2+bedprofile/4;
    assy = assembly_on_top ? "on top" : "sandwiched" ;
    echo(str("Bed wood size (",assy,"): ", l,"x",w));
    
    translate([0,0,-bedprofile/2]){
        xprofile(true); mirror([0,1,0]) xprofile();
        yprofile(); mirror() yprofile();
    }
    
    
    x_offset = (l - heated_bed_length)/2 - bedprofile - screw3_angle_center;
    
    
    color("LightGrey") translate([x_offset,0,heated_bed_thickness/2+bed_level_slack]) cube([heated_bed_length,heated_bed_width,heated_bed_thickness], center=true);
    
    support_beam();
//    translate([-120,0,0]) rotate([0,0,180]) support_beam();
    mirror([0,1,0]) support_beam();
    
//    color("peru") translate([0,0,assembly_on_top?bed_assembly_thickness/2:-bedprofile/2]) cube([l,w,bed_assembly_thickness], center=true);
}

module xprofile(endstop_adjuster=false) { 
    color("silver") translate([0,-disty/2+bedprofile/2,0]) rotate([0,90,0]) 2020Profile(lx); 
    xclamps(); mirror() xclamps(endstop_adjuster);
}
module yprofile() {
    color("silver") translate([-lx/2-bedprofile/2,0,0]) rotate([0,90,90]) 2020Profile(ly); 
//    yclamps(); mirror([0,1,0]) yclamps();
}

module xclamps(endstop_adjuster=false) {
    inner_clamp = bed_clamps_size()[1]-bed_clamps_size_outer();
    translate([lx/3,disty/2+inner_clamp,-bed_clamps_size()[2]/2]) bed_clamps(endstop_adjuster);
    internal = external - profilewidth*2;
    translate([0,internal/2-nema17_size(nema_length)[0]/2,0]) leadscrew_nut_clamp();
}

module yclamps() {
    inner_clamp = bed_clamps_size()[1]-bed_clamps_size_outer();
    translate([lx/2+inner_clamp+bedprofile,ly/3,-bed_clamps_size()[2]/2]) rotate([0,0,-90]) bed_clamps();
}

module support_beam() {
    
    dist = 4; //distance of holes from the edge of the bed.
    
    bed_corner = [(bed_platform_length)/2 - heated_bed_length - screw3_angle_center, heated_bed_width/2];
    support_hole = bed_corner + [dist, -dist];
    
    o = bedprofile/6;
    2o = 2*o;
    
    beam_polygon = [
        [beam_line_x(disty/2-bedprofile, o) , disty/2-bedprofile],
        [beam_line_x(disty/2-bedprofile, -2o), disty/2-bedprofile],
        [-bed_platform_length/2             , beam_line(-bed_platform_length/2,-2o)],
        [-bed_platform_length/2             , beam_line(-bed_platform_length/2, o)],
    ];
    
    difference(){
        translate([0,0,-bedprofile]) linear_extrude(bedprofile) 
            polygon(beam_polygon);
        translate([-bedprofile/2,-0,-bedprofile/2]) translate(beam_polygon[1]) rotate([90,0,0]){
            screw_hole();
        }
        translate([0,bedprofile/2,-bedprofile/2]) translate(beam_polygon[2]) rotate([0,90,0]){
            screw_hole();
        }
        translate([support_hole[0],support_hole[1], -bedprofile/2]) rotate(-45) {
            nutcatch_sidecut(str("M",bed_screws));
            cylinder(d=bed_screws, h=bedprofile);
        }
    }
    module screw_hole() {
        cylinder(d=bed_screws+1, h=bedprofile);
        translate([0, 0, bedprofile/2-bed_screws]) cylinder(d=bed_screws+4, h=bedprofile);
    }
    //Linear equation angled 45° 
    function beam_line_general(x, x1, y1, offset) = (x - x1+offset)+y1+offset;

    //Linear equation angled 45° that starts from an inside edge of the bed to its perpendicular edge, 
    //and passes from the support hole of the bed.
    function beam_line(x, offset=0) = beam_line_general(x, support_hole[0], support_hole[1], offset);

    //This call is equivalent to calling this: 
    //function beam_line_general_x(y, x1, y1, offset) = (y - y1-offset)+x1-offset;
    //solves the linear equation as per x
    //The slope=1 so the equation is symmetrical
    function beam_line_x(y, offset=0) = beam_line_general(y, support_hole[1], support_hole[0], -offset);
}


    function bed_size() = [lx,disty,0];