include <general-values.scad>
include <calculated-values.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
use <mechanical.scad>
use <lib/utl.NEMA.scad>

thickness = 6;
rod = 8;
bolt = 8;

mtol = 1;

save_material = true;

//z_axis_clamp();
//translate([-40,0,0]) z_axis_screw_clamp();


print(true, thickener = false);
//z_axis_clamp_internal(endstop = true);

//rotate([0,90,0]) endstop_thickener();

_height = bolt+8;
fastener_width=_height*3+bolt*2;
rodt = rod + mtol/2;
boltt = bolt+mtol;

fins = thickness*2;

lead_screw_clamp_height = externalZ - nema_length - nema_shaft - lead_screw_length - (profilewidth-_height)/2;

//nema17();

//print_leadscrew_clamp(height=lead_screw_clamp_height, pillow_thickness=3);


module print(endstop = false, thickener = true){
    rotate ([0,-90,0]) translate([thickness+rodt/2,0,0]) z_axis_clamp(endstop, print = true);
    if (thickener) translate([10,30,0]) rotate([0,90,0]) endstop_thickener();
}

module print_leadscrew_clamp(height, pillow_thickness){
    rotate([180,0,0]) z_axis_screw_clamp(height, pillow_thickness);
}

module z_axis_clamp(endstop = false, print = true){
    z_axis_clamp_internal(endstop, print = print);
}

function z_axis_clamp_size() = [thickness+rodt+fins,fastener_width,_height];
function z_axis_clamp_x_mountpoint_offset() = rodt/2+thickness;

module z_axis_clamp_internal(endstop = false, print = true){
    ext_d = (rodt+thickness*2);
    difference(){
        //clamp
        union() {
            //fins
            translate([rodt/2,-3*thickness/2,0]) cube([fins,thickness,_height]);
            translate([rodt/2,1*thickness/2,0]) cube([fins,thickness,_height]);
            //pocket
            cylinder_height = endstop?externalZ-z_home-profilewidth/2+_height/2-endstop_tolerance:_height;
            difference(){
                hull(){
                    cylinder(d=ext_d, h=cylinder_height );
                    if (endstop) translate([-ext_d/2,-ext_d/2,0]) cube([thickness,ext_d,cylinder_height]);
                }
                cylinder(d=rodt, h=cylinder_height);
                translate([0,-thickness/2,0]) cube([thickness*2,thickness,cylinder_height]);
            }
            if (endstop){
                translate([-ext_d/2,endstop_width/2+endstop_offset,cylinder_height-endstop_height]) {
                    % if (!print) translate([-endstop_thickness,0,0]) rotate([180,90,0]) rotate([0,0,90]) import("lib/MakerBot_Endstop.stl");
                    translate([0,-endstop_width,0]) cube([5,endstop_width,endstop_height]);
                    if (!print) endstop_thickener();
                }
                
            }
            
        }
        //screw
        h=(thickness*2+rodt)*2;
        translate([rodt/2+thickness,h/2,_height/2]) rotate([90,0,0]) cylinder(d=3+mtol, h=h);
        
        //nut & head traps
        translate([rodt/2+thickness,thickness,_height/2]) rotate([90,0,0]) nutcatch_parallel("M3", l=3);
        translate([rodt/2+thickness,-thickness,_height/2]) rotate([90,0,0]) cylinder(d=6, h=3);
    }
    plate();
}

module plate(offset=rodt/2+thickness){
    difference(){
        translate([-offset,-fastener_width/2,0]) cube([thickness, fastener_width, _height]);
        //screw
translate([-offset-e,fastener_width/4+thickness/2+bolt/3,_height/2]) rotate([0,90,0]) cylinder(d=boltt, h=thickness+2e);
        translate([-offset-e,-fastener_width/4-thickness/2-bolt/3,_height/2]) rotate([0,90,0]) cylinder(d=boltt, h=thickness+2e);

    }
}

module z_axis_screw_clamp(height=_height, pillow_thickness=3){
    plate(nema17_size(nema_length)[0]/2);
    bearing = bearing_size(lead_screw_bearing);
    bod = bearing[1];
    bh = bearing[2];
    height_diff = _height-height;
    translate([0,0,height_diff]) difference(){
        union(){
            cylinder(d=bod+pillow_thickness,h=height);
            translate([-bod/2,0,_height/2-height_diff]) cube([bod, bod+pillow_thickness, _height], center=true);
        }
        cylinder(d=bod+0.5,h=save_material?height:bh);
    }
}

module endstop_thickener(){
    difference(){
        translate([-ext_d/2,endstop_width/2+endstop_offset,cylinder_height-endstop_height]) {
            translate([-endstop_thickness,-endstop_width,0]) cube([endstop_thickness,endstop_width,endstop_height]);
            %translate([-endstop_thickness,0,0]) rotate([180,90,0]) rotate([0,0,90]) import("lib/MakerBot_Endstop.stl");
        }
        if (makerbot_endstop) rotate([0,90,0]) {
            translate([-3,-3,-15]) cylinder(d=4,h=30);
            translate([-14,-17,-15]) cylinder(d=4,h=30);
            translate([-14,-3,-15]) cylinder(d=4,h=30);
            translate([-14,-36,-15]) cylinder(d=4,h=30);
        }
    }
}

