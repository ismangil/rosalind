use <angle-fastener.scad>
include <general-values.scad>
use <alu-profile-40.scad>

module acrylic_angles_bottom(){
    AngleFastener([3,35,20],profilewidth/2,8,profilewidth/2,4, stiffeners = 2);
}

module acrylic_angles(){
    AngleFastener([3,profilewidth-acrylic_position,20],profilewidth/4,8,profilewidth/2,4, stiffeners = 2);
}

//rotate([0,90,0]) acrylic_angles_bottom();
//rotate([0,90,0]) acrylic_clip();

module profile(){
    translate([0,0,-profilewidth+1]) rotate([0,90,0]) rotate(90) AluProfile40(80);
}
height = 30;
width = 12;
thickness = 3;

//profile();

module acrylic_clip(){
    scale([1,1.2,1]) difference() { translate([0,acrylic_position,0]){
            cube([width,thickness,height]);
            hull(){
                    translate([0,0,height-2]) cube([width,thickness,2]);
                    translate([0,15,-6]) rotate([30,0,0]) cube([width,thickness,4]);
            }
            difference() {
                translate([0,0,-5]) cube([width,8,11]); 
                translate([0,8,-2]) rotate([30,0,0]) cube([width,7,10]);
            }
        }
        profile();
        translate([width/2, acrylic_position+thickness*2, width]) rotate([90,0,0]) cylinder(d=2, h=10);
    }
}

//acrylic_clip();

//translate([0,-profilewidth/2+0.5,-profilewidth-0.8]) rotate([90,0,0]) profile();
//rotate([0,90,0]) translate([0,-profile_slot_width/2,0]) slot_cover(15, 14, 1.9);

//rotate([0,90,0]) slot_cover_ribbon();
aluminium_cap();

module slot_cover(length=10, width=profile_slot_width, thickness=profile_slot_thickness){
    tolerance=-0.15;
    psw = width-tolerance;
    pst = thickness;
    flange = 3;
    tooth = 0.8;
    2f = 2*flange;
    translate([0,-flange,0]) cube([length, psw+2f, pst/2]);
    difference(){
        union(){
            translate([0,0,-pst]) cube([length, psw, pst]);
            translate([0,0,-pst-tooth/2]) rotate([0,90,0]) hull(){
                cylinder(d=tooth,h=length,$fn = 32);
                translate([0,psw,0]) cylinder(d=tooth,h=length,$fn = 32);
            }
        }
        translate([0,2,-pst-tooth]) cube([length, psw-4, pst+tooth/2]);
    }
    
}

module slot_cover_ribbon(){
    slot_cover();
    difference(){
        translate([0,-3,0]) cube([10,14.60,4]);
        translate([0,-2,1]) cube([10,12.60,2]);
        translate([0,2.5,1]) cube([10,3,4]);
    }
}

module aluminium_cap(){
    cube([40,40,1], true);
    translate([0,0,3]) difference(){
        minkowski(){
            cube([23.9,13.60,7], true); \\[25.9,15.60,7]
            cylinder(d=2, h=0.1);
        }
        cube([23,13,8], true);
    }
}