include <general-values.scad>
use <mechanical.scad>
use <z-axis-clamp-and-leadscrew-pillow-block.scad>
use <lib/utl.NEMA.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
use <bed.scad>

//for bigger structures add more stiffness by clamping at
//the bottom of the bed too.

rigid = external>400?true:false;

sthick = 6;
thicker = 3;

length = (external - profilewidth*2 - z_axis_clamp_x_mountpoint_offset()*2 - bed_size()[1])/2;

echo(str("length: ",length));
echo(str("Padding: ",length-_length));

height = bedprofile;
width = bedprofile*2;

bolts = 5;

print();

module print(screw_nut = false, endstop_adjuster = false){
    leadscrew_nut_clamp();
}

module leadscrew_nut_clamp(){
    translate([0,0,-bedprofile/2]) leadscrew_nut_clamp_internal();
}

module leadscrew_nut_clamp_internal(){
    half_nema = nema17_size(nema_length)[0]/2;
    mv = [0,-(external-2*profilewidth-bed_size()[1]-nema17_size(nema_length)[0])/2,0];
    difference(){
        translate(mv) union() {
            rounded_corner(length);
            mirror([1,0,0]) rounded_corner(length);;
            translate([-width/4,0,0]) cube([width/2,length,height]);
            translate([-width/2,0,0]) cube([width,length/2,height]);
        }
        hole();
        screws();
    }
}

function leadscrew_nut_clamp_size() = [width,length*2,height];

module rounded_corner(size = length){
    hull() {
        translate([width/4,size-width/4,0]) cylinder(d=width/2, h=height);
        translate([-width/2,0,0]) cube([width/4, width/4/2, height]);
    }
}

module screws(){
    mirror([1,0,0]) screw();
    screw();
}

module screw(screw_length = 100){
    y = length/2+0.5;
    translate([width/3,y,height/2]) rotate([90,0,0]) {
        translate([0,0,bolts]) cylinder(d=bolts+mtol, h=screw_length);
        cylinder(d=9+mtol, h=bolts);
    }   
}

module nutcatch(){
    translate([width/3,-length,-height/2]) rotate([90,0,0]) nutcatch_sidecut(str("M",bolts), l=width/3, clh=0.2, clsl=0.2);
}

module hole(tolerance = 1){
    screw_nut_hole(tolerance);
}

module screw_nut(tolerance = 1){
    cylinder(d=10.2+tolerance, h=14.90);
    translate([0,0,1.30]) cylinder(d=21.82+tolerance, h=3.60);
}

module screw_nut_hole(tolerance = 1){
    cylinder(d=leadscrew_nut_shaft_d+tolerance, h=40);
}