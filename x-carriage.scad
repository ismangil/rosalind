include <general-values.scad>
use <mechanical.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
use <x-axis-clamp.scad>

use <lib/functions.scad>

sthick = minimum_thickness;
length = 60;

screws = 3;
screwst = screws + mtol;

onepiece = false;
lmu = true;
tensioner_block_cap_thickness = 3;

_wwide = Xrods+2*sthick-screws;
_w = onepiece?Xrods+sthick-screws:_wwide;

width = ceil(_w/5)*5+screws; //round to nearest 5mm so that screws don't protrude
echo(str("Width: ",width));


echo(str("Height: ",height));

_height = x_carriage_rod_distance;

tensioner_slot_thickness = belt_thickness*1.3;
tensioner_slot_width = belt_width*1.3;

height = max(x_carriage_rod_distance+_wwide+2*sthick, 2*(tensioner_slot_width+x_axis_clamp_height()/2+belt_distance+belt_distance-0.5*(tensioner_slot_width-belt_width)+tensioner_slot_width)+sthick);

if (tensioner_thickness>width/2) echo("<font color=red><b>ERROR:</b> x-carriage too thin</font>");

e = 0.01;
2e = 2*e;

//x_carriage();

print();
//tensioner(nut = true);

module x_carriage(rails=true){
    mi = flip_secondary_axis?[0,1,0]:[0,0,0];
    rotate(180) translate([-length/2,0,0]) mirror(mi){
        front();
        back();    
        tensioner_block();
        if (rails) %translate([120,0,0]) x_rails();
        if (!onepiece) mirror([0,1,0]) pcb_slot();
    }
}

module print(){
    if (onepiece){
        rotate([0,270,0]) whole();
        rotate([90,0,0]) translate([-length-10,(_wwide+width)/2,-20]) {
            tensioner_block();
            translate([100,0,0]) tensioner_block_cap();
        }
    } else {
        rotate([90,0,0]) front();
        translate([length+20,0,0]) rotate([-90,0,0]) back();
        translate([-length-10, 10, -width]) rotate([90,0,0]) pcb_slot();
        rotate([90,0,0]) translate([-length-10,width/2,0]) tensioner_block();
        translate([0,55,_wwide/2]) rotate([90,0,0]) tensioner_block_cap();
    }
    
}

module back(){
    intersection() {
        whole();
        cut();
    }
}

module front(){
    difference() {
        whole();
        cut();
    }
}

module cut(){
    translate([-e,-e,-height/2-e]) cube([length+2*e, width/2+2*e, height+2*e]);
}

module whole(){
    difference() { 
        translate([0,-width/2,-height/2]) cube([length,width,height]);
//        union(){
//            translate([0,-width/2,-height/2]) cube([length,width,height]);
            //translate([0,0,-height/2]) cube([length,width/2,height]);
//            mirror([0,0,1]) roundness();
//            roundness();
//        } 
        //translate([120,0,0]) x_rails(mtol*2);
        
        //Lower rail space is bigger so that the carriage moves freely even if 
        //aluminium rods are not completely parallel
        
        //bushing tolerance is also bigger, see `bushings()`
        translate([120,0,0]) {
            x_rail(onepiece?0.2:mtol*3);
            mirror([0,0,1]) x_rail(onepiece?0.25:mtol*2); 
        }
        
        screws();
        if (!onepiece) 
            if (lmu){
                translate([0,0,_height/2]) rotate([0,90,0]) LM10LUU(1,length=100);
                translate([0,0,-_height/2]) rotate([0,90,0]) LM10LUU(1,length=100);
            }else
                bushings(mtol);
        
        //positioning spheres
        positioning_spheres(false);
        
        //single_bolt
        translate([length/2, width/2+e, 0]) rotate([90,0,0]) cylinder(d=modular_x_bolt+mtol, h=width+2e);
        translate([0,-tensioner_block_cap_thickness,0]) cap_screws();
    }
    if(onepiece) mirror([0,1,0]) pcb_slot();
    endstop_extension();
    
}
//positioning_spheres(true);
module positioning_spheres(centered=true, tolerance=0){
    trans=centered?[-length/2, -width/2+e, 0]:0;
    translate(trans){
        translate([0,0,2*height/5]) two_spheres(tolerance=tolerance);
        two_spheres(tolerance=tolerance);
    }
    module two_spheres(tolerance=0){
        translate([length/4,width/2,-height/5]) sphere(d=modular_x_bolt+tolerance);
        translate([3*length/4,width/2,-height/5]) sphere(d=modular_x_bolt+tolerance);
    }
}

module tensioner_block(){
    intersection(){
        translate([0,-_wwide/2-width/2,-height/2]) cube([length,_wwide/2,height*.45]);
        _tensioner_block();
    }
}

module _tensioner_block(){
    difference(){
        translate([0,-_wwide/2-width/2,-height/2]) cube([length,_wwide/2+tensioner_block_cap_thickness,height*.45]);
        //tensioner 
        translate([0,0,-tensioner_slot_width-x_axis_clamp_height()/2]){
            translate([0-e,-width/2,0]) tensioner();
            diff = belt_distance-0.5*(tensioner_slot_width-belt_width);
            translate([0-e,-width/2,-belt_distance-diff]) tensioner();   
            translate([length,-width/2,0]) mirror([1,0,0]){
                translate([0-e,0,0]) tensioner();
                translate([0-e,0,-belt_distance-diff]) tensioner();
            }
        }
        screws();
        nut_h = nut_height(str("M",screws));
        //cap screws
        cap_screws();
        
        translate([length/2, -width/2-e, 0]) rotate([90,0,0]) cylinder(d=modular_x_bolt*3, h=_wwide/2+2e);    
//       translate([length/2, -width-e, 0]) rotate([90,0,0]) rotate(30) nutcatch_parallel(str("M",modular_x_bolt), l=width/2+2*e);
    }
}

module cap_screws(){
    t = tensioner_block_cap_thickness;
    translate([2.5*length/8, -width/2+t+e, -height/8]) screw();
    translate([5.5*length/8, -width/2+t+e, -height/8]) screw();
    translate([2.5*length/8, -width/2+t+e, -3.5*height/8]) screw();
    translate([5.5*length/8, -width/2+t+e, -3.5*height/8]) screw();

    module screw(){
        rotate([90,0,0]){
            translate([0,0,-t]) cylinder(d=6, h=4);
            cylinder(d=3.5, h=20); 
            translate([0,0,_wwide/2+3+2*e]) nutcatch_parallel("M3",l=3,clk=0.1);
        }
    }
        
}

module tensioner_block_cap(){
    translate([0,-_wwide/2,0]) intersection(){
        translate([0,-width/2,-height/2]) cube([length,tensioner_block_cap_thickness,height*.45]);
        _tensioner_block();
    }
}

module bushings(tolerance=0,tolerance_h=ctol){
    two_bushings(tolerance,tolerance_h=ctol);
    mirror([0,0,1]) two_bushings(tolerance*2,tolerance_h=ctol);
    
    module two_bushings(tolerance=0,tolerance_h=ctol){
        translate([length/2,0,0]) {
            bushing(tolerance,tolerance_h=ctol);
            mirror([1,0,0]) bushing(tolerance,tolerance_h=ctol);
        }    
    }
    module bushing(tolerance=0,tolerance_h=ctol){
        translate([length/2-bushings_h-sthick,0,-x_carriage_rod_distance/2]) rotate([0,90,0]) cylinder(d=bushings_d+tolerance, h=bushings_h+tolerance_h);
    }
}

module tensioner(nut=false){
    module screw(){translate([0,-w/2,h/2]) rotate([0,90,0]) cylinder(d=tensioner_screw, h=2*(sthick+2e));}
    module tensioner_space(lrect = lrect, tolerance = 0){
        translate([sthick,-w,0]) cube([ lrect-sthick-tolerance, w-tolerance, h-tolerance]);
        translate([lrect-tolerance,(-w-tolerance)/2,0]) cylinder(d=w-tolerance, h=h-tolerance); 
    }
    
//    tolerance=nut?0.5:0;
    

    l = 0.45*length;
    lrect = l - tensioner_thickness/2;
    w = tensioner_thickness;
    h = tensioner_slot_width;
    
        if (nut == false) {
            //slot
            translate([0,-w,0]) cube([ l, tensioner_slot_thickness, h]); 
            //belt clamp
            translate([0,-tensioner_slot_thickness/2,0]) cube([ l/2, tensioner_slot_thickness/2, h]);
            //screw hole (maybe add a nutrap)
            screw();
            tensioner_space();
        } else {
            difference() {
                tensioner_space(tensioner_screw*4, tolerance=0.5);           
                screw();
            }
        }
        
}


module screws(){
    if (!onepiece) two_screws();
    if (!onepiece) translate([0,0,height/2-sthick]) two_screws();
    //extra screws for belt tensioner
    translate([0,0,-height/8]) two_screws(long=true);
    if (!onepiece) translate([0,0,height/2-sthick]) two_screws();
    translate([0,0,-(height/2-sthick)]) two_screws(long=true);
    
    module two_screws(long){
        translate([1*length/6,0,0]) screw(long);
        translate([5*length/6,0,0]) screw(long);
    }
    module screw(long=false){
        translate([0,width/2-e,0]) rotate([90,0,0]) {
            screw_length = long?width+_wwide/2:width;
            cylinder(d=screwst, h=screw_length);
            //head space
            translate([0,0,screw_length-screws]) cylinder(d=screws+3, h=screws);
            //nuts
            nut_h = nut_height(str("M",screws));
            translate([0,0,nut_h-2e]) nutcatch_parallel(str("M",screws), l=nut_h);
        }
    }
}

module pcb_slot(){
    p = x_carriage_pcb_thickness;
    w = 2;
    translate([0,width/2,modular_x_bolt*1.5]) {
        difference(){
            cube([length,p+2*w,x_carriage_pcb_height+6]);
            translate([-1,w,3]) cube([length+2,p,x_carriage_pcb_height]);
            translate([-1,p,w+p]) cube([length+2,p+w,x_carriage_pcb_height-w-p]);
        }
    }
}

module endstop_extension(){
    w=20;
    t=5;
    h=10;
    
    y=onepiece?-_wwide/2:-width/2;
    translate([length,y,_height/2-h]) cube([15,t,h]);
}

module roundness(){
   translate([0,0,_height/2]) rotate([0,90,0]) cylinder(d=width, h=length);
}

function x_carriage_size() = [length,width,height];