use <angle-fastener.scad>

//AngleFastener();

//61.78

//rotate([0,90,0]) translate([external/2-profilewidth,profilewidth,0]) AngleFastener([2.5,50,36],33,8);
    
//    rotate([0,0,90]){
//        rotate([0,90,0]) translate([external/2-profilewidth,profilewidth,0]) AngleFastener([2.5,50,36],33,8);
//    }
//external=500;
//profilewidth=40;
//module XYangles(){
//    rotate([0,0,-90]) translate([-profilewidth,profilewidth,0]) AngleFastener([2.5,50,36],33,8);
//    rotate([0,0,90]) translate([external-profilewidth,profilewidth-external,0]) AngleFastener([2.5,50,36],33,8);
//    rotate([0,0,180]) translate([-profilewidth,profilewidth-external,0]) AngleFastener([2.5,50,36],33,8);
//    rotate([0,0,0]) translate([external-profilewidth,profilewidth,0]) AngleFastener([2.5,50,36],33,8);
//}
//    
//
//XYangles();

size = [6.5,50,36];

AngleFastener(size, 33, 8);
use <x-axis-clamp.scad>
translate([-10,30,0]) rotate ([0,0,90]) x_axis_clamp();