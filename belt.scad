module belt(points, height=6, thickness=1){
        for (i = [0:1:len(points)-2]){
            hull(){
//                translate([point[0],point[1],0]) cylinder(d=thickness,h=height);
                translate([points[i][0],points[i][1],0]) cylinder(d=thickness,h=height);
                translate([points[i+1][0],points[i+1][1],0]) cylinder(d=thickness,h=height);
            }
        }
}

//belt([[300,100],[10,17],[20,20],[23,30]],10,1);
//belt([[3,10],[5,20]],10,1);
