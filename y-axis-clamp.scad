use <lib/ISOThreadUM2.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
include <general-values.scad>
use <lib/utl.NEMA.scad>
use <x-axis-clamp.scad>
use <extruder.scad>
use <mechanical.scad>
use <lib/electrical.scad>
use <x-carriage.scad>
 
//The "with endstop" version cannot be printed without support if `flip_secondary_axis` is false. Either enable supports on your slicer, or set one of the two options below to true.
support = false; //set this to true to make this part easier to print
two_parts = true; //another way to make easier to print

//2.15
rod = 10;
bolt = 8;
mtol = 1; //hole tolerance
height = 30;
center = false;
thickness = clamp_thickness;
slab_thickness = 10;

support2 = two_parts?false:support;
angle_height = y_clamps_angle_height;
slab_height = y_clamps_slab_height;

bolt_min_dist = 22;
nema = nema17_size(nema_length)[0];
idler_shelf_t = belt_distance-idler_height+belt_width;

//calc
slab_width=max(bolt*4,bolt_min_dist*1.75,x_axis_clamp_height()/2+Yrods/2+idler_height+idler_shelf_t+idler_flange_thickness);
rodt = rod + mtol;
boltt = bolt+mtol;
ifcenter = center ? slab_width/2 : 0;
bolt_Z = angle_height+profilewidth/2; //bolt position on slab
idler_hole_offset = idler_flange_diameter+2*mtol; //needs tolerance so that flange doesn't touch the angled part
idler_offset = [-rod/2-slab_thickness-idler_hole_offset,-slab_width+thickness,0];

//print(with_endstop=true);
//print_endstop_holder();
//translate([40,0,0]) mirror() y_axis_clamp_internal(false, with_idler=true);
//y_axis_clamp_internal(false, with_idler=true);
//y_axis_clamp();
//intersection(){
//    y_axis_clamp_internal(false, with_endstop=true);
//    translate([-500,-500,-500]) cube([1000,1000,500]);
//}
//translate([6,-3,-20]) cube([10,6,30]);

y_axis_clamp_internal(with_idler=true, with_endstop=false);

y_home =y_axis_clamp_y_min()+extruder_size()[1]+extruder_drive_thickness;
y_total = reverse_secondary_axis?
    max(angle_height, endstop_height):
    y_home-angle_height;

module print(with_idler=false, with_endstop){
    difference(){
        y_axis_clamp_internal(true, with_idler, with_endstop);
        if (two_parts&&!flip_secondary_axis)  translate([-rod/2-slab_thickness,-slab_width+thickness,0]){
            print_endstop_holder(prongs=true);
        }
    }
    if (two_parts&&!flip_secondary_axis) translate([30,-slab_width+rod,0]) rotate([0,-90,0]) print_endstop_holder();
}

module y_axis_clamp(idler=false, endstop=false){
    translate([0,bolt_Z,0]) rotate([90,0,0]) y_axis_clamp_internal(idler, endstop);
}

module y_axis_clamp_internal(with_idler=false, with_endstop=false){
    rod_clamp(with_endstop);
    if (with_idler) {
        idler();
        difference(){
            slab(with_endstop = with_endstop);
            belt_slot();
        }
    } else {
        slab(with_endstop = with_endstop);
    }
}

module idler(){
    module hole(){
        cylinder(minimum_thickness,d=idler_inner_diameter);
    }
    module transform(){
        rotate([90,0,0]) translate([0,profilewidth/4,-idler_shelf_t]) children();
    }
    
    
    translate(idler_offset) {
        difference(){
            cube([idler_hole_offset,idler_shelf_t,profilewidth/2]);
            transform() hole();
            %transform() {
                translate([0,0,idler_shelf_t]) cylinder(h=idler_height,d=idler_diameter);
                translate([0,0,-idler_height]) cylinder(h=idler_height,d=idler_diameter);
            }
        }
        transform() {
            difference () {
                cylinder(idler_shelf_t,profilewidth/4,profilewidth/4);
                hole();
            }
        }
    }
    
}

module belt_slot(){
    x = -slab_thickness-rod/2-profilewidth; //where profilewidth is just a sufficiently big number
    y = -slab_width+idler_shelf_t+belt_width*0.9+idler_flange_thickness;
    z = profilewidth/4 + idler_diameter/2 - belt_thickness/2;
    
    translate([x,y,z]) cube([profilewidth*2, belt_width*1.2, belt_thickness*2]);
}

module slab(angle=true, slab_height=slab_height, slab_width=slab_width, with_endstop=false){
    //angle
    if (angle) translate([-profilewidth/3-rod/2-slab_thickness,-slab_width+thickness,0]) hull(){
        translate([profilewidth/3,0,0]) cube([0.01,slab_width,thickness]);
        translate([0,0,angle_height]) cube([profilewidth/3,slab_width,0.01]);
    }
    
    translate([-slab_thickness-rod/2,center?-slab_width/2:-slab_width+thickness,0]) {
        difference(){
            //slab
            translate([0,0,0]) cube([slab_thickness, slab_width, slab_height]);
            //bolts
            translate([0,0,bolt_Z]){
                translate([0,slab_width/4,0]) rotate([0,90,0]) cylinder(d=boltt, h=height);
                translate([0,3*slab_width/4,0]) rotate([0,90,0]) cylinder(d=boltt, h=height);
            }
        }
        //if (print==false) translate([-7,0,0]) bolts();
        if (with_endstop) endstop_holder();
    }
}

module endstop_holder(){
    clearance=flip_secondary_axis?0:rod;
    translate([slab_thickness,-clearance,-y_total]) rotate(90) 
        cube([slab_width+clearance,slab_thickness+2e,y_total]);
    if (!flip_secondary_axis) translate([slab_thickness,endstop_width-clearance,-y_total+endstop_height]) rotate([0,90,0]) rotate([0,0,-90]){
        %import("lib/MakerBot_Endstop.stl");
        endstop_holes();
    }
}

module print_endstop_holder(prongs){
    difference(){
        endstop_holder();
        if (!prongs) holes(4);
    }
    if (prongs) holes();
    module holes(diameter=3){
        translate([slab_thickness/2,endstop_width/4,-y_total-e]) {
            cylinder(d=diameter, h=nema-angle_height+e+angle_height/2);
            translate([0,endstop_width/2,0]) cylinder(d=diameter, h=nema-angle_height+e+angle_height/2);
        }
    }
}

module rod_clamp(with_endstop=false){
        endstop_modifier=with_endstop&&(flip_secondary_axis||support2)?y_total:0;
        difference(){
        //rod holder
        translate([0,0,-endstop_modifier]) union() {
            //ears
            translate([rodt/2,-3*thickness/2]) cube([thickness*2,thickness,height+endstop_modifier]);
            translate([rodt/2,1*thickness/2]) cube([thickness*2,thickness,height+endstop_modifier]);
            //circle
            difference(){
                cylinder(d=rodt+thickness*2, h=height+endstop_modifier );
                cylinder(d=rodt, h=height+endstop_modifier);
                translate([0,-thickness/2,0]) cube([thickness*2,thickness,height+endstop_modifier]);
            }
            //endstop_holder
            if(with_endstop&&flip_secondary_axis) translate([-endstop_width/2,rodt/2,0]) {
                difference(){
                    cube([endstop_width,thickness,endstop_height]);
                }
                
                translate([0,endstop_thickness-1,endstop_height]) rotate([-90,0,0]){
                    %import("lib/MakerBot_Endstop.stl");
                    endstop_holes();
                }
            }
            
            //stiffness triangle
            translate([-thickness/2,0-rodt,0])  cylinder(r=thickness, h=height+endstop_modifier, $fn=3);
        }
        //screw
        h=(thickness*2+rodt)*2;
        translate([rodt/2+thickness,h/2,height/2]) rotate([90,0,0]) cylinder(d=3+mtol, h=h);
        
        //nut & head traps
        translate([rodt/2+thickness,thickness,height/2]) rotate([90,0,0]) nutcatch_parallel("M3", l=3);
        translate([rodt/2+thickness,-thickness,height/2]) rotate([90,0,0]) cylinder(d=6, h=3);
        if (!flip_secondary_axis) translate([-rodt/2,-slab_width+rodt/2,-y_total]) cube([endstop_thickness*2,endstop_width+10,endstop_height]);
    }
}

module bolts(){
    translate([25,slab_width/4,slab_height/2]) rotate([0,90,0]) rotate([0,180,0]) hex_bolt(dia=boltt, hi=height);
    translate([25,3*slab_width/4,slab_height/2]) rotate([30,0,0]) rotate([0,90,0]) rotate([0,180,0]) hex_bolt(dia=boltt, hi=height);
}


function y_axis_clamp_thickness() = thickness;
function y_axis_slab_thickness() = slab_thickness;
function y_axis_clamp_size() = [thickness*2+rodt+slab_thickness*2.5,slab_height,slab_width+minimum_thickness];
function y_axis_clamp_offset() = rod/2+slab_thickness;

function y_axis_clamp_y_min() = angle_height;
function y_axis_clamp_idler_offset() = idler_offset;