use <lib/functions.scad>
include <general-values.scad>

module LM10LUU(tolerance=0, length=55){
    color("grey") cylinder(d=19+tolerance, h=length+e);
}

module LM8UU(tolerance=0){
    cylinder(d=14+tolerance, h=24);
}

function LM8UUsize() = [14, 14, 24];

//627ZZ(true);

bearing(name="627", hq=true, flanged=1);

translate([30,30,0]) idler();

module 627ZZ(hq=false){
    bearing(7,19,7,hq);
}

module 688Z(tolerance=0, hq=false){
    bearing(8-tolerance,16+tolerance,5+tolerance,hq);
}

module bearing(id, od, h, hq=false, name=undef, flanged=false){
    
    if (value(name, bearings)==undef) echo(str("<font color='red'>ERROR: no such bearing ",name," </font>"));
    
    id=name!=undef?value(name, bearings)[0]:id;
    od=name!=undef?value(name, bearings)[1]:od;
    h=name!=undef?value(name, bearings)[2]:h;
    
    if (hq&&!flanged){
        difference(){
            hull(){
                translate([0,0,0]) cylinder(d=od-1,h=1);
                translate([0,0,1]) cylinder(d=od,h=1);
                translate([0,0,h-1]) cylinder(d=od,h=1);
                translate([0,0,h]) cylinder(d=od-1,h=1);
            }
            hull(){
                translate([0,0,-1])  cylinder(d=id+1,h=1);
                translate([0,0,+1]) cylinder(d=id,h=0.01);
            }
            hull(){
                translate([0,0,h]) cylinder(d=id,h=1);
                translate([0,0,h+1]) cylinder(d=id+1,h=1);
            }
            translate([0,0,-5]) cylinder(d=id,h=h+5);
        }
        
    } else {
        difference(){
            union(){
                if (flanged) {
                    if (flanged==1||flanged==3) cylinder(d=od+2, h=1);
                    if (flanged==2||flanged==3) translate([0,0,h-1]) cylinder(d=od+2, h=1);
                }
                cylinder(d=od,h=h);
            }
            cylinder(d=id,h=h);
        }
    }
}


module idler(idler_diameter = 12, idler_height=8.5, idler_flange_diameter = 16, idler_flange_thickness = 1){
    cylinder(d=idler_flange_diameter, h=idler_flange_thickness);
    cylinder(d=idler_diameter, h=idler_height);
    translate([0,0,idler_height-idler_flange_thickness]) cylinder(d=idler_flange_diameter, h=idler_flange_thickness);
}

function bearing_size(name) = value(name, bearings);

bearings = [
//    name   id od  h
    ["608", [8, 22, 7] ],
    ["627", [7, 19, 7] ],
    ["688", [8, 16, 5] ],
    ["623", [3, 10, 4] ],
    ["LM10UU", [10, 20, 22.5] ],
    ["LM10LUU", [10, 20, 55] ],
];

module coupler(from=5,to=8){
    color("LightGrey") cylinder(h=25, d=18);
}
