include <general-values.scad>
use <mechanical.scad>
use <z-axis-clamp-and-leadscrew-pillow-block.scad>
use <lib/utl.NEMA.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
use <bed.scad>

//for bigger structures add more stiffness by clamping at
//the bottom of the bed too.

rigid = external>400?true:false;

sthick = 6;
thicker = 3;

_length = LM8UUsize()[1]/2+sthick+thicker;

minimum_length = (external - profilewidth*2 - z_axis_clamp_x_mountpoint_offset()*2 - bed_size()[1])/2;

length = _length < minimum_length ? minimum_length : _length;

echo(str("length: ",length));
echo(str("Padding: ",length-_length));

height = bedprofile;
width = bedprofile*2;

bolts = 5;

preview(true);

//print(endstop_adjuster = true);
//rotate([90,0,0]) thickener();

module print(endstop_adjuster = false){
    translate([0,0,0]) back();
    translate([0,10,0]) front(endstop_adjuster);
}

module preview(endstop_adjuster = false){
    back();
    front(endstop_adjuster);
}

module section(height){
    rounded_corner(_length, height);
    mirror([1,0,0]) rounded_corner(_length,height);
    translate([-width/4,0,0]) cube([width/2,_length,height]);
    translate([-width/2,0,0]) cube([width,_length/2,height]);
}

module front(endstop_adjuster = false){
    half_nema = nema17_size(nema_length)[0]/2;
    height=rigid?height*2:height;

    translate([0,0,rigid?-height/2:0]) difference(){
        section(height);
        hole();
        screws();
        translate([-width/2,-length,0]) cube([width,length,height]);
    }
    if (endstop_adjuster){
        eaw = width/2;
        eath = minimum_thickness;
        translate([-eaw/2,_length,height/2-eath]) difference(){
            union() {
                cube([eaw,endstop_thickness,eath]);
//                section(eath);
            }
            translate([eaw/2,endstop_thickness/2,-1]) {
                cylinder(d=3, h=20);
                translate([0,0,3]) nutcatch_parallel("M3", l=5);
            }
        }
        
    }
}

module bed_clamps(endstop_adjuster = false){
    front(endstop_adjuster);
    back();
}

function bed_clamps_size() = [width,length+_length,height];
function bed_clamps_size_outer() = _length;

module rounded_corner(size = length, h = height){
    hull() {
        translate([width/4,size-width/4,0]) cylinder(d=width/2, h=h);
        translate([-width/2,0,0]) cube([width/4, width/4/2, h]);
    }
}

module back(){
    half_nema = nema17_size(nema_length)[0]/2;
    
    height=rigid?height*2:height;
    translate([0,0,rigid?-height/2:0]) difference(){
        union(){
            translate([-width/2,-length,0]) cube([width,length,height]);
            if (rigid){
                //extra part
                translate([-width/2,-length-bedprofile,0]) cube([width,bedprofile,height/2]);
            }
        }
        hole();
        screws();
    }
    
    
}

module screws(){
    short_screw_l = ceil((_length-bolts+length)/5)*5+1;
    l = rigid?short_screw_l:100;
    mirror([1,0,0]) screw(l);
    screw(l);
    if (rigid) translate([0,0,height]){
        //second pair of screws, with nutcatch
        mirror([1,0,0]) screw();
        screw();
        nutcatch();
        mirror([1,0,0]) nutcatch();
        
        //vertical screws
        yoffset=-length;
        translate([0,yoffset,0]) rotate([90,0,0]) screw();
        mirror([1,0,0]) translate([0,yoffset,0]) rotate([90,0,0]) screw();
    }
}

module screw(screw_length = 100){

    translate([width/3,_length,height/2]) rotate([90,0,0]) {
        translate([0,0,bolts]) cylinder(d=bolts+mtol, h=screw_length);
        cylinder(d=9+mtol, h=bolts);
    }   
}

module nutcatch(){
    translate([width/3,-length,-height/2]) rotate([90,0,0]) nutcatch_sidecut(str("M",bolts), l=width/3, clh=0.2, clsl=0.15);
}

module hole(tolerance = 1){
    if (rigid)
        translate([0,0,height]) LM8UU(tolerance);
    LM8UU(tolerance);
}


module thickener(amount=length-_length){
    difference(){
        translate([-width/2,-length,0]) cube([width,amount,height]);
        translate([0,0,-height]) screws();
    }
}