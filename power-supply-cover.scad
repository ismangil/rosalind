inner_dims = [99,41,30];

thickness = 2;

t = thickness;
psu = inner_dims;

difference() {
    cube(psu+[2*t,2*t,t]);
    translate([t,t,t]) cube([psu[0], psu[1], psu[2]+2*t]); ;
}
