external=522;
externalZ = 500;
profilewidth=40;
bedprofile=20;
profile_slot_width=8.4;

bed_platform_length = 280; //length or "auto"
bed_platform_width = 401; //length or "auto"

heated_bed_width = 213;
heated_bed_length = 214;
heated_bed_thickness = 3;

bed=300;

//FIXME: when true the belts are not visualized correctly on
flip_secondary_axis=false; 

reverse_secondary_axis=flip_secondary_axis;

smallAngleFastenerHeight = 40;
bigAngleFastenerThickness = 2.5;
FrameAngleFastenerLength = 50; 

clamp_thickness = 6;

Yrods = 10;
Xrods = 10;
Zrods = 8;

nema_length = 48;
extruder_nema_length = nema_length;

acrylic_position = profilewidth/4;
acrylic_thickness = 3;

mtol=0.5; //bolt tolerance (big)
ctol=0.2; //clamp tolerance (small)

minimum_thickness = 7;

frame_bolts = 8;
bed_screws = 3;

tapered_angle_fastener_bolts = true;
angle_fastener_bolts = frame_bolts;

motor_shaft_hole = 24;
nema_shaft = 21; //22

leadscrew = [8,8,400];
leadscrew_nut_d = 22;
leadscrew_nut_h = 15;
leadscrew_nut_flange_thickness = 4;
leadscrew_nut_shaft_depth = 9;
leadscrew_nut_shaft_d = 10.2;

y_linear_bearing = "LM10UU";

lead_screw_bearing = "688";
lead_screw_length = 400;

x_carriage_pcb_width = 60;
x_carriage_pcb_height = 40;
x_carriage_pcb_thickness = 6;

y_carriage_bearing = "623";
idler_bearing = "608"; //deprecated
idler_diameter =  12;
idler_height = 8.5;
idler_inner_diameter = 3;
idler_flange_diameter = 16;
idler_flange_thickness = 1;

//motor_cogged_wheel_solid_part_thickness 
mcwspt = 5;

//motor cogged wheel teeth offset
teeth_offset = 14;

belt_width = 6;
belt_thickness = 1.36;
belt_pitch = 2;

bushings_d = 11.95;
bushings_h = 9.64;

modular_x_bolt = 8;

tensioner_thickness = 8;
tensioner_screw = 3;

profile_slot_thickness = 3.65;
//how much does the extruder protrude below the x-carriage?
extruder_offset = 25;

//endstop stuff
endstop_height = 16.2;
endstop_width = 40;
endstop_thickness = 7.5;
//how much is the switch off center?
endstop_offset = 10;
makerbot_endstop = true;
//how much tolerance to leave for the z endstop adjuster screw
endstop_tolerance = 10;

//bed assembly thickness
insulation_thickness = 3; //set to 0 if insulation does not protrude from the bed frame
glass_thickness = 3; //set to 0 if no glass
heated_bed_pcb_thickness = 2;
bed_assembly_thickness = insulation_thickness + glass_thickness + heated_bed_pcb_thickness;

// drive gear
extruder_bearing = "608";
drive_gear_hobbed_radius = 6.35 / 2;
drive_gear_hobbed_offset = 3.2;
drive_gear_length = 13;
drive_gear_tooth_depth = 0.2;

//globals
y_clamps_angle_height = profilewidth/2;
y_clamps_slab_height = profilewidth/2+acrylic_position+y_clamps_angle_height;
x_carriage_rod_distance = 2*Yrods+4*minimum_thickness+Xrods;
echo(str("x_carriage_rod_distance = ", x_carriage_rod_distance ));
belt_distance = minimum_thickness;

extruder_drive_thickness = minimum_thickness + drive_gear_length - drive_gear_hobbed_offset - 0.5;

e=0.01;
2e=2*e;
