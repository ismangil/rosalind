use <mechanical.scad>
include <general-values.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
use <x-carriage.scad>
use <lib/functions.scad>
use <belt.scad>


mtol = 1; //bolt tolerance
rail_tolerance = 0.2;

sthick = ceil(minimum_thickness/2);
bearingHeight = bearing_size(y_carriage_bearing)[2];
bearingOD = bearing_size(y_carriage_bearing)[1];
bearingID = bearing_size(y_carriage_bearing)[0];
nuth = 5;

lpw = 30; //left part width

lpl = 55-e;
bearingRoom = 20;

clamp_screws = 3;
lmuPos = 20;
lmuHeight = bearing_size(y_linear_bearing)[1];

alu_rod_d = 10;
alu_rod_dist = x_carriage_rod_distance;

lmuZ = 0;
bearing2Z = sthick+mtol+bearingHeight+mtol+sthick+mtol;

screw_end = bearingHeight+2*mtol+sthick+sthick+nuth;
screw_end2 = bearing2Z+bearingHeight+sthick+2*mtol;

alurodZ = x_carriage_rod_distance/2;
lph = lmuHeight+5*sthick; //violating the thickness rule so that the belt stays horizontal

//I could have just made it equal in height with the clamp (lmuHeight+5*sthick) but this way assembly is easier (there's a slot so you don't need to hold the linear bearing while you screw the clamp

alurodY = lpl-alu_rod_d-sthick;

bottom = alurodZ-alu_rod_d-alu_rod_dist;

clamp_slot_height = 2;
clamp_bolts = 3; //M3
clamp_bolt_head = 5.5; //M3

clamp_width = lpl-alurodY+alu_rod_d/2+sthick;

//print(side="left", clamp=true);
//translate([100,0,0]) print("right");
x_axis_clamp(false, side="right", position=80);
translate ([0,100,0]) x_axis_clamp(false, side="left", position=80);

function x_axis_clamp_size() = [lpw,lpl,lph];

module print(side="left", clamp=true, body=true){
    mi = side=="left"?[0,0,0]:[0,1,0];
    mirror(mi){
        if (body) translate([0,0,lpw]) rotate([90,90,90]) body(side);
        if (clamp) translate([20,60,lpw]) rotate([0,-90,90]) translate([-lpw,0,-screw_end2]) clamp();
    }
}
//preview
module x_axis_clamp(original_coords=false, side="left", position = (external-2*profilewidth)/2){
    mi = side=="left"?[1,0,0]:[0,0,0];
    trans2 = side=="left"?[-lpw,0,0]:[0,0,0];
    trans = original_coords?[0,0,0]:[lpw/2,alurodY,0];
    rot = original_coords?[0,0,0]:[0,0,180];
    translate(trans) rotate(rot){
        mirror(mi) translate(trans2){
            body(side, position = position);
            translate([-1,0,0]) clamp();
        }
        x_rails_positioned(0,side=side);
    }
}

module idler_shelves(){
    shelves_h = belt_width+belt_distance;
    shelves_w = idler_flange_diameter;
    translate([lpw-shelves_w,0,-lph/2+0.5-shelves_h]) {
        cube([shelves_w/2, 10, shelves_h]);
        cube([shelves_w, lpl-clamp_width+clamp_thickness/2, shelves_h-idler_height-1]);
    }
}

module body(side="left", position = (external-2*profilewidth)/2){
    x = x_axis_clamp_idler_center()[0];
    notchY = x_axis_clamp_idler_center()[1];
    sig = side=="right"?1:-1;
    tensionerY= alurodY-(x_carriage_size()[1]/2+tensioner_thickness+idler_diameter);
    difference(){
        union(){
            whole();
            idler_shelves();
        }
        clamp_cut(tolerance=0.2);        
        //bearing screw notch
        notchH = idler_height+minimum_thickness*2;
        transz = -(lph/2+0.1+idler_height+minimum_thickness);
        translate([x,notchY,transz]) { 
            cylinder(d=bearingID, h=notchH);
            translate([0,0,notchH]) nutcatch_sidecut(str("M",bearingID),x);
        }
    }
    %translate([x,notchY,-lph/2-0.1]) { 
        if (side=="left"){
            translate([0,0,-bearingHeight]) {
                bearing(name="623", flanged=2);
                translate([0,0,-bearingHeight]) bearing(name="623", flanged=1);
            }
            translate([0,0,-idler_height-belt_width-belt_distance]) idler(d=idler_diameter, h=idler_height);
        } else {
            translate([0,0,-idler_height]) idler(d=idler_diameter, h=idler_height);
            translate([0,0,-bearingHeight-belt_width-belt_distance]) {
                bearing(name="623", flanged=2);
                translate([0,0,-bearingHeight]) bearing(name="623", flanged=1);
            }
        }
        position=side=="left"?position:external-2*profilewidth-position;
        //belt
        belt_length = position-x_carriage_size()[0]/2+lpw/2;
        translate([0,bearingOD/2+belt_thickness,-(belt_width+1)]){
            d = -tensionerY;
            beltstart_up= side=="right"?[0,-idler_diameter]:[0,0];
            beltstart_down= side=="left"?[0,-idler_diameter]:[0,0];
            belt([beltstart_up,[-belt_length,d]]);
            translate([0,0,-belt_width-belt_distance]) belt([beltstart_down,[-belt_length,d]]);
        }
    }
    
    % if(side=="left") translate([endstop_height,0,lph/2]) rotate(90) import("lib/MakerBot_Endstop.stl");
}

module clamp(){
    intersection(){
        whole();
        clamp_cut(tolerance=-0.5);
    }
}

module whole(){
    mirror([0,0,1]) half(); 
    half();
}

module half(print = false){
    difference() {
        nut_h=5;
        top_of_clamp_screw = alurodZ+7;
        vch=alu_rod_d*2;
        screw_l=max(alu_rod_d,10);
        
        fortification_height = 6;
        union(){
            cube([lpw,lpl,lph/2]);
            h=alurodZ-lph/2;
            //rod clamps
            hull(){
                t=clamp_width;
                translate([0,lpl-t,lph/2]) cube([lpw,t,h]);
                d=alu_rod_d+sthick*2;
                translate([0,alurodY,lph/2+h]) rotate([0,90,0]) cylinder(d=d, h=lpw);
            }
            //vertical clamp screw support
            translate([lpw/2,lpl-(clamp_bolts+sthick)/2,lph/2+alu_rod_d]) cylinder(d=clamp_bolts+sthick, h=fortification_height);
        }
        screw_l2 = min(ceil5(screw_l),12);
        //vertical clamp screw holes
        translate([lpw/2,lpl-(clamp_bolts+sthick)/2,lph/2+alu_rod_d+fortification_height-3-screw_l2]) {
        //replace with         
        //translate([lpw/2,lpl-(clamp_bolts+sthick)/2,alurodZ+vch-25]) { //25mm screws
            //tightening screw hole
            cylinder(d=clamp_bolts, h=ceil5(screw_l));
            echo(str("Screw M3x",screw_l2));
            //space for tightening-screw head
            translate([0,0,ceil5(screw_l)-2]) cylinder(d=clamp_bolt_head, h=13);    
            //nut traps
            rotate([0,0,90]) nutcatch_sidecut("M3", l=10, clh=0.2, clsl=0.2);
        }
        
        translate([lpw/2,0,lmuZ]) rotate([-90,0,0]) LM10LUU();
        
        translate([0,0,lmuHeight/2+sthick]) clamp_screws();
        
        //upper rail clamp slot
        translate([0,alurodY,alurodZ]) cube([lpw,lpl-alurodY,clamp_slot_height]);
        
        //rod support
        x_rails_positioned(rail_tolerance);
        
    }
    if (!print) translate([lpw/2,-e,lmuZ]) rotate([-90,0,0]) LM10LUU();
}

module x_rails_positioned(rail_tolerance=0, side){
    x = side=="left"?external/2+lpw/2-1-lpw:0;
    color("Gainsboro") translate([x,alurodY,0]) x_rails(rail_tolerance);   
}
module x_rail(tolerance=0){
    length = external/2+lpw/2+1;
    translate([lpw+1-length,0,alurodZ]) rotate([0,90,0]) cylinder(d=alu_rod_d+tolerance, h=length, $fn = 64); 
}

module x_rails(tolerance=0){
    mirror([0,0,1]) x_rail(tolerance); x_rail(tolerance);
}

module clamp_screws(){
            //clamp screws
        translate([-mtol,10,0]) rotate([0,90,0]) cylinder(d=clamp_screws+mtol, h=lpw+2*mtol);
        translate([-mtol,lpl-10,0]) rotate([0,90,0]) cylinder(d=clamp_screws+mtol, h=lpw+2*mtol);
        //nut traps
        m3_h=2;
        translate([m3_h,10,0]) rotate([0,90,0]) nutcatch_parallel("M3", l=m3_h);
        translate([m3_h,lpl-10,0]) rotate([0,90,0]) nutcatch_parallel("M3", l=m3_h);
        //head cones
        translate([lpw-m3_h,10,0]) rotate([0,90,0]) cylinder(d=5+mtol, h=m3_h);
        translate([lpw-m3_h,lpl-10,0]) rotate([0,90,0]) cylinder(d=5+mtol, h=m3_h);
}

module clamp_cut(tolerance=0){
    translate([0,0,-lmuHeight/2-sthick*2-tolerance/2]) cube([lpw/2,lpl,lmuHeight+4*sthick+tolerance]); //other part
}

echo("alurodY",alurodY);

function x_axis_clamp_height() = lph;
function x_axis_clamp_width() = lpw; 
function x_axis_clamp_size() = [lpl,lpw,lph]; 
function x_axis_clamp_only() = clamp_width;
function x_axis_clamp_idler_center() = [lpw-idler_diameter/3,lpl-clamp_width-idler_diameter];
function x_axis_clamp_y_offset() = flip_secondary_axis?lpl-alurodY:alurodY;
