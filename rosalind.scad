/*

     Rosalind: A parametric CoreXY 3D printer
     Copyright (C) 2017  Michael Demetriou

     This file is part of Rosalind.

     Rosalind is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 2 of the License, or
     (at your option) any later version.

     Rosalind is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Rosalind. If not, see <http://www.gnu.org/licenses/>.   

                                                                            */

include <Fractional_T-Slot_20x_mm_OpenSCAD_Library/T-Slot.scad>
use <alu-profile-40.scad>
use <angle-fastener.scad>
use <x-axis-clamp.scad>
use <z-axis-clamp-and-leadscrew-pillow-block.scad>
include <lib/PRZutility.scad>
use <lib/utl.NEMA.scad>
use <bed.scad>
use <XY-motor-mount.scad>
use <x-carriage.scad>
use <y-axis-clamp.scad>
use <mechanical.scad>
include <calculated-values.scad>
include <general-values.scad>
use <extruder-drive.scad>
use <bowden-extruder.scad>
include <belt.scad>

internal = external-profilewidth*2;
internalZ = externalZ-profilewidth*2;

positionY = 100;
positionX = 100;
zposition = 0;

Y = reverse_secondary_axis?-positionY:positionY;

//echo (str("Z Home: ",z_home));
half_nema = nema17_size(nema_length)[0]/2;

// Z motor
translate([external/2,half_nema+profilewidth,0]) Zmotor();
translate([external/2,-half_nema+profilewidth+internal,0]) mirror([0,1,0]) Zmotor();

//powerbrick
translate([profilewidth,profilewidth+FrameAngleFastenerLength,0]) cube([240,130,50]);

frame();

//Y axis clamps
y_axis_clamps();

//z axes
zclamps();

//x-carriage
x_carriage_positioned();
//acrylic_and_angles();
motors();
bed_positioned();

extruder_motor_x = profilewidth+extruder_drive_size()[0]/2+FrameAngleFastenerLength;
extruder_motor_y = profilewidth;
extruder_motor_z = external-profilewidth-nema17_size()[0];

translate([extruder_motor_x, extruder_motor_y, extruder_motor_z]) rotate([-90,0,0]) rotate(90) compact_extruder(true, true);

translate([external-extruder_motor_x, extruder_motor_y, extruder_motor_z]) mirror([1,0,0]) rotate([-90,0,0]) rotate(90)  compact_extruder(true, true);

module Zmotor() {
    
        translate([0,0,nema_length]) nema17(height=nema_length);
        translate([0,0,nema_length+nema_shaft/2]) coupler();
        rotate(90) translate([0,0,externalZ-profilewidth/2-z_axis_clamp_size()[2]/2]) z_axis_screw_clamp();
        color("Gainsboro") translate([0,0,nema_length+nema_shaft]) cylinder(d=leadscrew[0], h=leadscrew[2]);
}

module frame(){
    translate([0,0,externalZ/2]){
        //Z
        AluProfile40(externalZ);
        translate([0,external-profilewidth,0]) AluProfile40(externalZ);
        translate([external-profilewidth,external-profilewidth,0]) AluProfile40(externalZ);
        translate([external-profilewidth,0,0]) AluProfile40(externalZ);
    }
    translate([(external)/2,0,externalZ]) rotate([0,90,0]){
        //X
        XYframe();
    }
    XYangles();
    translate([(external),(external)/2,externalZ]) rotate([0,90,90]){
        //Y
        XYframe();
    }
    translate([0,0,externalZ-profilewidth]){
        XYangles();
    }
}

module y_carriage(flip=false){
    x1 = flip?internal-y_axis_clamp_offset():y_axis_clamp_offset();
    x2 = flip?y_axis_clamp_offset():internal-y_axis_clamp_offset();
    pos = flip?positionX+x_home:x2-positionX+x_home;
    alurodY = (x_axis_clamp_size()[0]-x_axis_clamp_y_offset());
    translate([0,Y+y_home,0]){
        translate([x2, 0, 0]) {
            rotate(flip?180:0) x_axis_clamp(side="left", position=pos);
        }
        translate([x1, 0, 0]) {
            rotate(flip?180:0) x_axis_clamp(side="right", position=pos);
        
        }
    }
    
}


module y_axis_clamps(){
    2YaxisClamps();
    translate([external,0,0]) mirror([1,0,0]) 2YaxisClamps(endstop=true);

    //Y rods

    translate([profilewidth,profilewidth,external-YClampOffset]){
        translate([y_axis_clamp_offset(), 0, 0]) rotate([-90,0,0]) cylinder(d=10, h=430);
        color("Gainsboro") translate([internal-y_axis_clamp_offset(), 0, 0]) rotate([-90,0,0]) cylinder(d=Yrods, h=430);
        //x-axis clamp  (y-carriage)
        y_carriage(flip=flip_secondary_axis);
    }
}

module motors(){ //translate([profilewidth*1.5+bigAngleFastenerThickness,internal+profilewidth/2-bigAngleFastenerThickness,internal+2*profilewidth/3]) rotate([0,180,0]) nema17(height=nema_length);
    translate([0,0,0]) XYmotor();
    translate([external,0,-belt_distance-minimum_thickness]) mirror() XYmotor("down");
}

module acrylic_and_angles(){
    translate([profilewidth,acrylic_position,internal]) rotate([0,0,0]) mirror() AngleFastener([3,profilewidth-acrylic_position,20],profilewidth/4,8,4, stiffeners = 2);
}


module bed_positioned(){
    translate([external/2,external/2,z_home-zposition]) bed();
}

module x_carriage_positioned(){
    translate([profilewidth+y_axis_clamp_offset()+positionX+x_home, Y+y_home+profilewidth, external -YClampOffset]) {
        x_carriage(rails=false);
        if (flip_secondary_axis) translate([0,x_carriage_size()[1]/2,0]) bowden_extruder();
        else translate([0,-x_carriage_size()[1]/2,0]) mirror([0,1,0]) bowden_extruder();

    }
}

module XYmotor(side="up"){
    nema_offset = [ 3*profilewidth/4,
                    internal+profilewidth/2-bigAngleFastenerThickness,
                    externalZ-profilewidth-y_axis_clamp_offset()-y_axis_clamp_size()[2]-belt_distance-idler_height+belt_width ];
    
    translate(nema_offset) rotate([0,0,0]) {
        nema17(height=nema_length);
        translate([0,0,teeth_offset-2]) cylinder(d=idler_diameter, h=10);
        rotate(90) XY_motor_mount();
    }
    belts(side);
    
    module belts(){
        y_clamp_idler = [ profilewidth+y_axis_clamp_offset()+y_axis_clamp_idler_offset()[0],
                      5*profilewidth/4 ];
    
        x_clamp_idler = [ profilewidth+y_axis_clamp_offset()-x_axis_clamp_width()/2-idler_diameter/3+belt_thickness, 
                          positionY+y_home+profilewidth+x_axis_clamp_idler_center()[1] ];
        
        %translate([0,0,nema_offset[2]+teeth_offset]) {
            belt([[ nema_offset[0]-idler_diameter/2, nema_offset[1] ],  y_clamp_idler-[idler_diameter/2,0]]);
            
            rad = (idler_diameter/2 + belt_thickness/2);
            
            belt([y_clamp_idler-[0, rad],
                 [external-y_clamp_idler[0], y_clamp_idler[1]-rad]]);
            
            belt([nema_offset+[idler_diameter/2,0],x_clamp_idler]);
        }
        sig = side=="down"?-1:1;
        %translate([0,0,nema_offset[2]+teeth_offset+(-belt_distance-belt_width)*sig]) {
            belt([x_clamp_idler,
                 [profilewidth+y_axis_clamp_offset()+y_axis_clamp_idler_offset()[0]-idler_diameter/2, profilewidth/2-y_axis_clamp_idler_offset()[0]]]);
        }
    }
}

module zclamps(){
    zclampz = profilewidth/2-z_axis_clamp_size()[2]/2;
    translate([0,0,zclampz]) zclampslevel(true);
    translate([0,0,zclampz+internalZ+profilewidth+z_axis_clamp_size()[2]]) mirror([0,0,1]) zclampslevel(endstop=true);
}

module zclampslevel(rods=false, endstop=false){
    translate([external/2,0,0]){
        zclampsaxis(rods);
        translate([0,external,0]) mirror([0,1,0]){
           zclampsaxis(rods,endstop); 
        }
    }
}

module zclampsaxis(rods=false, endstop=false){
    zclamp(rods, endstop);
    mirror() zclamp(rods);
}

module zclamp(rod=false, endstop=false){
    zclampx = profilewidth+z_axis_clamp_x_mountpoint_offset();
    translate([-bed_x_clamps_offset(),zclampx,0]) rotate([0,0,90]) {
        z_axis_clamp(endstop);
        if (rod) color("silver") cylinder(d=Zrods, h=externalZ-profilewidth/2);
    }
}

module XYframe(){
    AluProfile40(external-2*profilewidth);
    translate([0,external-profilewidth,0]) AluProfile40(external-2*profilewidth);
    translate([externalZ-profilewidth,external-profilewidth,0]) AluProfile40(external-2*profilewidth);
    translate([externalZ-profilewidth,0,0]) AluProfile40(external-2*profilewidth);
    
    //angles
    // rotate([0,90,0]) translate([external/2-profilewidth,profilewidth,0]) AngleFastener([2.5,50,36],33,8);
    
}



module XYangles(){
    l = FrameAngleFastenerLength;
    rotate([0,0,-90]) translate([-profilewidth,profilewidth,0]) AngleFastener([2.5,l,36],33,8);
    rotate([0,0,90]) translate([external-profilewidth,profilewidth-external,0]) AngleFastener([2.5,l,36],33,8);
    rotate([0,0,180]) translate([-profilewidth,profilewidth-external,0]) AngleFastener([2.5,l,36],33,8);
    rotate([0,0,0]) translate([external-profilewidth,profilewidth,0]) AngleFastener([2.5,l,36],33,8);
}
    

module YAxisClampP(rotY=0,rotZ,idler = false, endstop = false){
    translate([profilewidth+y_axis_clamp_offset(),profilewidth/2,external-YClampOffset]) rotate([0,rotY,0]) rotate(rotZ) y_axis_clamp(idler, endstop=endstop);
}

module 2YaxisClamps(rotY, endstop=false){
    YAxisClampP(rotY, rotZ=0, idler = true, endstop = endstop?flip_secondary_axis:false);
    translate([0,external,0]) mirrory() YAxisClampP(rotY, rotZ=0, endstop = endstop?!flip_secondary_axis:false);  
}