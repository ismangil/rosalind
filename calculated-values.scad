include <general-values.scad>
use <x-axis-clamp.scad>
use <y-axis-clamp.scad>
use <x-carriage.scad>
use <extruder.scad>

//y_carriage_block_height = bearing_size(y_linear_bearing)[1] + 5 * ceil(minimum_thickness/2);

YClampOffset = (max(nema_length+1/3*profilewidth+x_axis_clamp_height()/2+10,profilewidth+smallAngleFastenerHeight));

y_min = y_axis_clamp_y_min()+x_carriage_size()[1]/2+extruder_size()[1]+extruder_drive_thickness;
y_max = external - 2*profilewidth - y_axis_clamp_y_min() - max(y_axis_clamp_y_min(), endstop_height) - x_axis_clamp_y_offset();

echo("aaa",-y_axis_clamp_y_min() - max(y_axis_clamp_y_min(), endstop_height));

z_home = external-YClampOffset-x_carriage_size()[2]/2-extruder_offset-bed_assembly_thickness;
y_home = flip_secondary_axis?y_max:y_min;
x_home = x_axis_clamp_size()[1]/2+x_carriage_size()[0]/2; //this starts from the center of the Y rods


//y_max = external - profilewidth - y_min;

//echo(extruder_drive_size());

echo(str("Z home = ", z_home));
echo(str("Y home = ", y_home));


//unused
//bottom_Y_rail_Z = external-YClampOffset-x_carriage_rod_distance/2-bedprofile/2;