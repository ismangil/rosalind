use <lib/Polygon.scad>

use <mechanical.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>

 translate([lpw-bearingRoom,7,screw_end]) nut("M5");
 
     translate([7,7,3]) nutcatch_parallel("M7");;//nut
