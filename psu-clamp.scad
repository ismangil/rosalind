psu = [99,150,41];
hole_dist = 17.46;
lower_hole_z = 8.16;
hole_diam = 3;
bolt_diam = 8;

tol=2;
shorter=7;

rotate([90,0,0]) 
psuClamp();

module psuClamp(){
    difference(){
        cube([60,3,psu[2]-shorter]);
        translate([0,0,0]){
            translate([40,-tol,19]) rotate([-90,0,0]) cylinder(h=30,r=bolt_diam/2+0.5);
            
            translate([15,-tol,lower_hole_z]) rotate([-90,0,0]) cylinder(h=30,r=hole_diam/2+0.25);
            translate([15,-tol,lower_hole_z+hole_dist]) rotate([-90,0,0]) cylinder(h=30,r=hole_diam/2+0.25);
    }
    }
}