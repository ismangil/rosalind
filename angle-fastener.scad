
size = [2.5,50,36];
tol=.5;

//AngleFastener(size,33,10.9, taper=4);

rotate([0,90,0]) AngleFastener([2.5,60,20],30,3+tol, stiffeners=1, taper=0);



module AngleFastener(size, hole_position, hole_diameter, hole_position2 = undef, hole_diameter2 = undef, stiffeners = 0, taper = 0){
    hole_diameter2 = hole_diameter2==undef ? hole_diameter : hole_diameter2;
    hole_position2 = hole_position2==undef ? hole_position : hole_position2;
    translate([-size[0],0,0]) face(size, hole_position, hole_diameter, taper);
    
    translate([0,size[0],size[2]]) rotate([0,180,90]) face(size, hole_position2, hole_diameter2, taper);
    if (stiffeners > 0) {
        linear_extrude(height = 2) polygon([[-size[1],0],[0,0],[0,size[1]]]);
        translate([0,0,size[2]-stiffeners]) linear_extrude(height = stiffeners) polygon([[-size[1],0],[0,0],[0,size[1]]]);
    }
}

module face(size, hole_position, hole_diameter, taper = 0){
    difference(){
        cube(size);
        for(i=[hole_position/2:hole_position:size[1]-hole_position/2]) {
            translate([-0.01, i,size[2]/2]) rotate([0,90,0]) cylinder(d=hole_diameter+taper, d2=hole_diameter, h=size[0]+0.02);
        }
    }
}

function AngleFastenerSize() = size;