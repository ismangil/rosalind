include <general-values.scad>
use <angle-fastener.scad>
use <lib/utl.NEMA.scad>
use <y-axis-clamp.scad>

//difference(){
//    AngleFastener(size2,33,11, taper=5);
//    translate([0,0,1]) AngleFastener(size,33,10.9);
//}

taper=tapered_angle_fastener_bolts?4.5:0;
hole=tapered_angle_fastener_bolts?11:angle_fastener_bolts+mtol*2;
slab_height = y_clamps_slab_height;
mounting_plate_width=profilewidth-acrylic_position;

mx=-nema17_size(nema_length)[0]/2;
my=nema17_size(nema_length)[0]/2;
mz=nema17_size(nema_length)[2];



//translate([-nema17_size(nema_length)[0]/2,-nema17_size(nema_length)[0]/2,0])

left=false;

mi = left?[0,0,0]:[1,0,0];
    mirror(mi) print();
//preview();

module preview(){
    XY_motor_mount();
//    XY_motor_mount(top=false);
    nema17(nema_length);
}


module XY_motor_mount(){
    translate([-mx,-my,-mz]){
        XY_motor_mount_internal(top=true);
        XY_motor_mount_internal(top=false);
    }
}

module print(){
    rotate([0,0,0]) XY_motor_mount_internal();
    rotate([0,180,0]) translate([0,nema17_size(nema_length)[0]+20,-nema_length]) XY_motor_mount_internal(true);
}

module XY_motor_mount_internal(top=false){
    txx = nema17_size(nema_length)[0]/2;
    tzz = top?(nema_length)/2:-minimum_thickness/2;
    
    difference(){
        union() {
            translate([0,0,tzz]) mounting_plate(top);
            translate([-txx*2,0,tzz]) mirror([1,0,0]) mounting_plate(top);
        }
        
        if (top) {
            pin(0.1);
            translate([-txx*2,0,0]) mirror([1,0,0]) pin(0.2);
        }
    }

    //plates
    translate([mx,my,0]) difference(){
        XY_motor_mount_angle(top=top);
        
        translate([0,0,nema_length*1.5]) nema_holes( nema17_holes()[0]+mtol, nema17_holes()[1]+mtol,nema_length*2);
    }
}

module mounting_plate(top){
    difference(){
        cube([mounting_plate_width,y_axis_slab_thickness(),(nema_length+minimum_thickness)/2]);
        //hole
        tz_hole = profilewidth/2;
        if (!top) translate([profilewidth/2, 0, tz_hole]) rotate([-90,0,0]) cylinder(d=angle_fastener_bolts+mtol, h=15);
    }
    if (!top) pin();
}

module pin(tolerance=0){
    d=y_axis_slab_thickness()/2+tolerance;
    translate([3*profilewidth/4-d, d, nema_length/2]) cylinder(d=d, h=15);
}

module XY_motor_mount_angle(top=true){
    xy = nema17_size(nema_length)[0];
    h = minimum_thickness/2;
    if (top)
        translate([0,0,mz+h/2]) difference(){    
            cube([xy,xy,h], center=true);
            translate([0,0,-5]) cylinder(d=motor_shaft_hole, h=20);
        }
    else
        translate([0,0,-h/2]) cube([xy,xy,h], center=true);
}
