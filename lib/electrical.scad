module endstop_holes(depth=5, out=true){
    diam=out?3.5:3;
    translate([3,2.5,0]) cylinder(d=diam,h=out?1:depth, $fn=12);
    translate([3,13.5,0]) cylinder(d=diam,h=out?1:depth, $fn=12);
    translate([17,2.5,0]) cylinder(d=diam,h=depth, $fn=12);
    translate([36.5,2.5,0]) cylinder(d=diam,h=depth, $fn=12);
}