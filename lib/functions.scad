data=[ ["abc",1],["bca",2],["cad",3],["d",4],["a",5],["b",6],["c",7],["d",8],["e",9] ];

//find values from a hashtable by their keys
function value(key, data) = data[search([key],data)[0]][1];

echo (value("877", data));

function typeof(a) = a==undef   ?"undefined":
                  len(a)==0     ?"empty":
                  a[0]==undef   ?"number":
                  a[0]+0==undef ?"string":
                  "vector";


include <nutsnbolts/data-access.scad>;                
include <nutsnbolts/data-metric_cyl_head_bolts.scad>; 
function nut_height(key) = data_screw_fam[search([key],data_screw_fam)[0]][9];
function ceil5(number) = ceil(number/5)*5;

//thanks trygon (http://forum.openscad.org/Determining-what-data-type-a-variable-is-holding-td16111.html)

module rotate_about(v,a) {
translate(v) rotate(a) translate(-v) child(0);
}