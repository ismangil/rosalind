include <general-values.scad>
use <lib/mendel90/vitamins/e3d_hot_end.scad>
use <x-carriage.scad>
use <lib/nutsnbolts/cyl_head_bolt.scad>
include <lib/mendel90/conf/config.scad>

length = 50;
height = 85;

min_length = x_carriage_size()[0]/2+2*modular_x_bolt;
min_height = 2*x_carriage_size()[2]/3+2*modular_x_bolt;

clamp_thickness = 15.375;
clamp_length = 25.625;
clamp_width = 14.350;


if (length<min_length) echo("<font color=red>WARNING: extruder length smaller than positioning spheres distance</font>");
if (height<min_height) echo("<font color=red>WARNING: extruder height smaller than positioning spheres distance</font>");

l = max(min_length, length);
h = max(min_height, height);
w = minimum_thickness*2;


bowden_extruder(dual=true);
//translate([0,-x_carriage_size()[1]/2,0]) x_carriage();
//print();

screw_l = 50;
cap_l = 40.7;
cap_w = 31;
cap_h = 5.5;

z_position = 39;

module print(dual=true){
    translate([(dual?-l:0),-60,w]) rotate([-90,0,0]) slab(dual);
    translate([cap_l/2+30, -w, -z_position+cap_h/2]) connector();
    translate([cap_l/2+30, -w-40, cap_w*2-1.4]) rotate([-90,0,0]) funnel();
//    translate([cap_l/2+30, -w+20, z_position+cap_h/2]) rotate([-180,0,0]) funnel();
}

module bowden_extruder(dual=false){
    translate([0,0,-x_carriage_size()[2]/2]) {
        slab(dual);
        single();
        if (dual){
            translate([l,0,0]){
                mirror() single();
            }
        }
    }
    
}

module single(){
    z_offset = 41.7;
    connector();
    %translate([0,w+clamp_thickness,z_offset]) rotate([0,0,180]) e3d_hot_end([e3d,   "HEE3DCLNB: E3D clone aliexpress",66,  6.8, 16,    46, "lightgrey", 12,    5.6,  15, [1, 5,  -4.5], 14.5, 21]);
    funnel();
    color("DarkGrey") translate([0,w+11/2+cap_w*1.5,z_offset-20]) rotate([90,0,0]) fan(fan40x11);
}

module funnel(){
    difference(){
        union(){
            translate([0,w,z_position+1]) rotate([0,0,180]) import("lib/E3D_print_fan_funnel.stl");
            translate([0,w+cap_w/2,z_position-cap_h/2]) {
                difference(){
                    translate([ cap_l/2, cap_w, e]) rotate([0,0,180]) cube([cap_l,cap_w,cap_h]);
                    cylinder(d=12+ctol, h=30);
                    
                }
            }
        }
        screw(); mirror([1,0,0]) screw();
        screwcaps(); mirror([1,0,0]) screwcaps();
    }   
    module screwcaps() {
        c = 7;
        translate([9.75-c/2,screw_l-e,z_position-c/2]) cube([c,screw_l-cap_w,c]);
    }
}


module connector(tol=0){
    difference(){
        translate([-cap_l/2-tol/2, w-3-tol/2, z_position-cap_h/2-tol/2]) cube([cap_l+tol, cap_w/2+3+tol, cap_h+tol]);
        screw(); mirror([1,0,0]) screw();
        translate([0,w+cap_w/2,z_position-cap_h/2-e]) cylinder(d=12+ctol, h=30);
    }
}

module slab(dual){
    x_offset = dual?l/2:0;
    bolt_z = x_carriage_size()[2]/2;
    difference(){
        union(){
            translate([-l/2,0,0]) cube([l+(dual?l:0),w,h]);
            translate([x_offset,0,bolt_z]) positioning_spheres(tolerance=-0.6);
        }
        translate([x_offset,-e,bolt_z]) rotate([-90,0,0]) cylinder(d=modular_x_bolt+mtol, h=30+2e);
        translate([x_offset,w-minimum_thickness+e,bolt_z]) rotate([90,0,0]) nutcatch_parallel(str("M",modular_x_bolt), l=minimum_thickness);
        nutcatch(); mirror([1,0,0]) nutcatch();
        screw(); mirror([1,0,0]) screw();
        connector(0.5);
        if (dual){
            translate([l,0,0]){
                nutcatch(); mirror([1,0,0]) nutcatch();
                screw(); mirror([1,0,0]) screw();
                connector(0.5);
            }
        }
    }
}

module nutcatch(){translate([9.75,-e-4,z_position]) rotate([90,0,0]) nutcatch_parallel("M3", l=7.5);}
module screw()  translate([9.75,0,z_position]) rotate([-90,0,0]) cylinder(d=4, h=screw_l);

module chamfer(){
    a=clamp_length/2;
    d=(cap_l-clamp_length)/2;
    linear_extrude(cap_h*2) polygon(points=[[a,0-e],[a+d+e,0-e],[a+d,cap_w/2]]);
}