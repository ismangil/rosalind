use <lib/ISOThreadUM2.scad>

scale([0.95,0.95,1]){
    longBearingBolt();
    translate([20,0,0]) shortBearingBolt();
}
module shortBearingBolt(height=26.5){
    thread = 5;
    
    rolson_hex_head(6); //head
    cylinder(d=7,h=height-thread,$fn=24); //smooth part for bearings
    translate([0,0,height-thread]) thread_out(6,thread,20); //M6 thread
    translate([0,0,height-thread]) thread_out_centre(6,thread);
}

module longBearingBolt(height = 31.5){
    rolson_hex_head(6); //head
    cylinder(d=7,h=height,$fn=24); //smooth part for bearings
}