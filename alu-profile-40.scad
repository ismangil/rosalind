include <general-values.scad>
rotate([90,180,90]) AluProfile40(30);
rotate([90,180,90]) translate([profilewidth,profilewidth,0]) AluProfile40(30);

module AluProfile40(height){
    color("silver") linear_extrude(height = height, center = true, convexity = 0)
        import("lib/alu_section_40.dxf");
}
